Projet utilisant le framework Angular.

# Intro

Pour l'utiliser :
- Installer Angular
- Ce rendre dans le dossier front
- Exécuter un "npm install"
- Lancer le projet avec "npm start"
- Se rendre à l'adresse "http://localhost:4200"

# Les fonctionnalités :

## Articles
- Lister les Articles
- Créer un article grâce au bouton en bas a gauche
- Avoir des informations sur l'article en cliquand sur la carte

## Commande
- Lister les commandes
- Créer une commande grâce au bouton en bas a gauche
