import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Command } from '../../models/command.model';
import { CommandsService } from '../../services/commands.service';

@Component({
  selector: 'app-create-command',
  templateUrl: './create-command.component.html',
  styleUrls: ['./create-command.component.scss']
})
export class CreateCommandComponent implements OnInit {

  public commandFormGroup = new FormGroup({
    productId: new FormControl(),
    commandPrice: new FormControl(),
    quantity: new FormControl(),
  });

  constructor(private commandService: CommandsService) { }

  ngOnInit(): void {
  }

  public onSave(): void {
    const value = this.commandFormGroup.value;

    const objectToSave: Command = { 
      id: Math.floor(Math.random() * 1000000),
      productId: value.productId,
      commandPrice: value.commandPrice,
      quantity: value.quantity,
      commandDate: new Date() 
    };

    this.commandService.postCommand(objectToSave);
  }

}
