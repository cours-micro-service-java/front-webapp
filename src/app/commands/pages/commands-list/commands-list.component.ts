import { Component, OnInit } from '@angular/core';
import { Command } from '../../models/command.model';
import { CommandsService } from '../../services/commands.service';

@Component({
  selector: 'command-commands-list',
  templateUrl: './commands-list.component.html',
  styleUrls: ['./commands-list.component.scss']
})
export class CommandsListComponent implements OnInit {

  public commands: Command[];

  constructor(private commandsService: CommandsService) { }

  ngOnInit(): void {
    this.loadCommands();
  }

  private loadCommands(): void {
    this.commandsService.getAllCommandes().subscribe((commands: Command[]) => {
      this.commands = commands;
    });
  }

  

}
