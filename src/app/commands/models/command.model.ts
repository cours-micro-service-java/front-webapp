export interface Command {
    id: number;
    productId: number;
    commandPrice: number;
    quantity: number;
    commandDate: Date;
}