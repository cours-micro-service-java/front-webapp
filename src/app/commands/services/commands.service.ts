import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Command } from '../models/command.model';

@Injectable({
  providedIn: 'root'
})
export class CommandsService {

  constructor(private httpClient: HttpClient, private router: Router) { }

  private apiEndPoint = 'http://localhost:8892/commandes-service/';

  public getAllCommandes() {
    return this.httpClient.get(`${this.apiEndPoint}commandes`);
  }

  public postCommand(command: Command) {
    return this.httpClient.post(`${this.apiEndPoint}commandes`, command).subscribe(result => {
      this.router.navigateByUrl('/commandes');
    });
  }
}
