import { Component, Input, OnInit } from '@angular/core';
import { Command } from '../../models/command.model';

@Component({
  selector: 'command-command-item',
  templateUrl: './command-item.component.html',
  styleUrls: ['./command-item.component.scss']
})
export class CommandItemComponent implements OnInit {

  @Input() command: Command;

  constructor() { }

  ngOnInit(): void {
  }

}
