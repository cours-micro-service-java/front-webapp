import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { CommandsListComponent } from './pages/commands-list/commands-list.component';
import { CommandItemComponent } from './components/command-item/command-item.component';
import { CreateCommandComponent } from './pages/create-command/create-command.component';
import { ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [
  { path: '', component: CommandsListComponent },
  { path: 'create', component: CreateCommandComponent}
];

@NgModule({
  declarations: [CommandsListComponent, CommandItemComponent, CreateCommandComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ]
})
export class CommandsModule { }
