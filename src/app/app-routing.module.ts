
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'articles', pathMatch: 'full' },
  { path: 'articles', loadChildren: () => import('./articles/articles.module').then(t => t.ArticlesModule) },
  { path: 'commandes', loadChildren: () => import('./commands/commands.module').then(t => t.CommandsModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
