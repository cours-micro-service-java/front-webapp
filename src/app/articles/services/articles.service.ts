import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Article } from '../models/article.model';

@Injectable({
  providedIn: 'root'
})
export class ArticlesService {

  constructor(private httpClient: HttpClient, private router: Router) { }

  private apiEndPoint = 'http://localhost:8892/articles-service/';

  public getAllArticles() {
    return this.httpClient.get(`${this.apiEndPoint}Articles`);
  }

  public postArticle(article: Article): void {
    this.httpClient.post(`${this.apiEndPoint}Articles`, article).subscribe(result => {
      this.router.navigateByUrl('/articles');
    });
  }

  public getArticleById(id: number) {
    return this.httpClient.get(`${this.apiEndPoint}Articles/${id}`);
  }
}
