import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ArticlesListComponent } from './pages/articles-list/articles-list.component';
import { ArticleCardComponent } from './components/article-card/article-card.component';
import { CreateArticleComponent } from './pages/create-article/create-article.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ArticleDetailsComponent } from './pages/article-details/article-details.component';

const routes: Routes = [
  { path: '', component: ArticlesListComponent },
  { path: 'create', component: CreateArticleComponent },
  { path: ':id', component: ArticleDetailsComponent}
];

@NgModule({
  declarations: [ArticlesListComponent, ArticleCardComponent, CreateArticleComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
})
export class ArticlesModule { }
