import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Article } from '../../models/article.model';
import { ArticlesService } from '../../services/articles.service';

@Component({
  selector: 'app-article-details',
  templateUrl: './article-details.component.html',
  styleUrls: ['./article-details.component.scss']
})
export class ArticleDetailsComponent implements OnInit {

  public article: Article;

  constructor(private route: ActivatedRoute, private articleService: ArticlesService) { }

  ngOnInit(): void {
    // Double subscribe pas très joli

    this.route.params.subscribe(params => {
      this.articleService.getArticleById(params.id).subscribe((article: Article) => {
        this.article = article;
      });
    });
  };

}
