import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Article } from '../../models/article.model';
import { ArticlesService } from '../../services/articles.service';

@Component({
  selector: 'article-articles-list',
  templateUrl: './articles-list.component.html',
  styleUrls: ['./articles-list.component.scss']
})
export class ArticlesListComponent implements OnInit {

  constructor(private articleService: ArticlesService, private router: Router) { }

  public articles: Article[] = [];

  ngOnInit(): void {
    this.loadArticles();
  }

  private loadArticles(): void {
    this.articleService.getAllArticles().subscribe((articles: Article[]) => {
      this.articles = articles;
    });
  }

  public itemClick(article: Article): void {
    this.router.navigateByUrl(`/articles/${article.id}`);
  }

}
