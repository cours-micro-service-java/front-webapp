import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Article } from '../../models/article.model';
import { ArticlesService } from '../../services/articles.service';

@Component({
  selector: 'app-create-article',
  templateUrl: './create-article.component.html',
  styleUrls: ['./create-article.component.scss']
})
export class CreateArticleComponent implements OnInit {

  public articleFormGroup = new FormGroup({
    name: new FormControl(),
    price: new FormControl()
  });

  constructor(private articleService: ArticlesService) { }

  ngOnInit(): void {
  }

  public onSave(): void {
    const value = this.articleFormGroup.value;
    const objectToSave: Article = { id:Math.floor(Math.random() * 1000000), nom: value.name, prix: value.price };
    this.articleService.postArticle(objectToSave);
  }

}
