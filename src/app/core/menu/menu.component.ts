import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'core-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  public buttonText = 'Nouvel élément';
  public url: string;

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        this.url = val.url
        switch (val.url) {
          case '/commandes':
          case '/commandes/create':
            this.buttonText = 'Nouvelle commande';
            break;
          case '/articles':
            this.buttonText = 'Nouvel article';
            break;
          default:
            this.buttonText = 'Nouvel élément';
            break;
        }
      }
    });
  }

  public btnAddClick(): void {
    this.router.navigateByUrl(`${this.url}/create`);
  }
}
